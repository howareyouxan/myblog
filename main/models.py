from django.db import models


class Portfolio(models.Model):
    image = models.ImageField(upload_to='uploads/portfolio/%Y/%m/%d')
    title = models.CharField(max_length=100)
    project = models.CharField(max_length=50)
    client = models.CharField(max_length=50)
    duration = models.CharField(max_length=50)
    technologies = models.CharField(max_length=50)
    budjet = models.CharField(max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Portfolio'
        verbose_name_plural = 'Portfolios'


class BlogPost(models.Model):
    image = models.ImageField(upload_to='uploads/Blog/%Y/%m/%d')
    title = models.CharField(max_length=100)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Blog'
        verbose_name_plural = 'Blogs'


class AboutMe(models.Model):
    image = models.ImageField(upload_to='uploads/About/%Y/%m/%d', default=True)
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)
    birthdate = models.CharField(max_length=255, blank=True)
    nationality = models.CharField(max_length=255, blank=True)
    experience = models.CharField(max_length=255, blank=True)
    address = models.CharField(max_length=255, blank=True)
    freelance = models.CharField(max_length=255, blank=True)
    languages = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    telegram = models.CharField(max_length=255, blank=True)
    instagram = models.CharField(max_length=255, blank=True)
    my_cv = models.CharField(max_length=255, blank=True)
    file = models.FileField(default=True, upload_to='files')

    def __str__(self):
        return self.first_name

    class Meta:
        verbose_name = 'Info'
        verbose_name_plural = 'Info'

class Experience(models.Model):
    about = models.ForeignKey(AboutMe, on_delete=models.CASCADE, related_name='experience_about')
    date = models.CharField(max_length=10, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField()


class Education(models.Model):
    about = models.ForeignKey(AboutMe, on_delete=models.CASCADE, related_name='education_about')
    date = models.CharField(max_length=10, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField()
