from django.views import generic
from . import models


# Create your views here.


class HomeView(generic.TemplateView):
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['portfolios'] = models.Portfolio.objects.all()
        context['blogs'] = models.BlogPost.objects.all()
        context['info'] = models.AboutMe.objects.first()
        return context


class BlogView(generic.ListView):
    model = models.BlogPost
    template_name = 'pages/blog.html'
    context_object_name = 'blogs'


class BlogDetailView(generic.DetailView):
    model = models.BlogPost
    template_name = 'pages/blogdetail.html'
    pk_url_kwarg = 'id'

