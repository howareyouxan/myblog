from django.contrib import admin

from . import models
from .models import Portfolio, BlogPost, AboutMe, Experience, Education

@admin.register(models.Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
    pass

@admin.register(models.BlogPost)
class BlogPostAdmin(admin.ModelAdmin):
   pass


class ExperienceAdmin(admin.TabularInline):
    model = Experience


class EducationAdmin(admin.TabularInline):
    model = Education


@admin.register(AboutMe)
class AboutMeAdmin(admin.ModelAdmin):
    inlines = [
        ExperienceAdmin,
        EducationAdmin,
    ]
