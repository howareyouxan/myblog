from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.HomeView.as_view(), name='home_url'),
    path('blog/', views.BlogView.as_view(), name='blog_url'),
    path('blog/<int:id>', views.BlogDetailView.as_view(), name='blogdetail_url'),
]
