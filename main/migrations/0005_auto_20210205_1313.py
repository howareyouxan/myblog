# Generated by Django 3.1.6 on 2021-02-05 13:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_comment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='active',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='updated',
        ),
        migrations.AddField(
            model_name='comment',
            name='image',
            field=models.ImageField(default=True, upload_to='uploads/Comment/%Y/%m/%d'),
        ),
    ]
